import express from 'express'
import { authenticateUser } from '../lib/user_util'
import { generateToken } from '../lib/token_util'

const router = express.Router()

const extractParams = params => {
    const errors = []
    if (!params.email) {
        errors.push({
            details: 'Mandatory parameter "email" is missing',
            status: 400
        })
    }
    if (!params.password) {
        errors.push({
            details: 'Mandatory parameter "password" is missing',
            status: 400
        })
    }
    if (errors.length)
        throw { errors }
    return params
}

const createErrRes = (err) => Array.isArray(err) ? err : [err]  

router.post('/', async (req, res) => {
    try {
        const params = extractParams(req.body)
        const user = await authenticateUser(params.email, params.password)
        const token = await generateToken(user.email)
        return res.json({ token })
    } catch (e) {
        return res.status(e.status || 500).json(createErrRes(e))
    }
})

export default router