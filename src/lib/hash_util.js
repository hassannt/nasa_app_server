import bcrypt from 'bcryptjs';

const salt = bcrypt.genSaltSync(10);
export function hashPassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(10, (err1, salt) => {
            bcrypt.hash(password, salt, (err2, hash) => {
                if (err2) return reject(err2)
                return resolve(hash)
            });
        });
    })
}

export function compareHash(password, hash) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, hash, (err, res) => {
            return err ? reject(err) : resolve(res)
        });
    })
}

