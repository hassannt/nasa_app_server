# NASA Demo app (server)

# Technical requirements!
 To be able to set up the project, the following installations must be made:
  - Nodejs (latest)
  - yarn (latest)
  - Mongodb (latest)
 
# Mongodb
The application uses mongodb as the database technology, the url to the mongodbb client, the name of the database and the collection used to store the application's can be found in the .env file at the root directory, please make sure that the url to your mongodb client and the name of the database and the collection used are updated in the .env file, then you can start the mongo client.

### Running the project

Make sure to install all dependencies, run
```sh
$ yarn install
```
after installing all the dependencies you should be ready to run the server, run
```sh
$ yarn run serve
```
and the server should  be up and running.

### Registering a user

To register a user (to be able to login to the application) please make a post request to
http://localhost:3000/register 
with the body submitted in json format like this: 
```sh
{
    email :'user@example.com',
    password: 'yawyaw'
}
```
```

That's it, the server is up and running and a user has been registered, you can check the client_app now.