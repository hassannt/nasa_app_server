import express from 'express'

import { updateUser } from '../lib/user_util.js'

const router = express.Router()

const extractParams = params => {
    const errors = []
    if (!params.favourites) {
        errors.push({
            error: 'Mandatory parameter "favourites" is missing',
            status: 400
        })
    }
    if (errors.length)
        throw errors
    return params
}

router.patch('/', async (req, res) => {
    try {
        const params = extractParams(req.body)
        await updateUser(req.userEmail, 'favourites', params.favourites)
        return res.json({
            message: 'User\'s favourites has successfully been registered'
        })
    } catch (e) {
        return res.status(e.status || 500).json({ error: e })
    }
})
import dotenv from 'dotenv'
dotenv.load()

export default router