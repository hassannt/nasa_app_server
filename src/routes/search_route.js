import express from 'express'
import axios from 'axios'
import { start } from 'repl';
const router = express.Router()

const NASA_SEARCH_ENDPOINT = 'https://images-api.nasa.gov/search'

const isValidYearFormat = () => true
const extractParams = (params) => {
    if (!params.query) {
        throw new Error({
            details: 'Mandatory parameter "query" is missing',
            status: 400
        })
    }
    if (params.year_start && !isValidYearFormat(params.year_start)) {
        throw new Error({
            details: 'parameter "year_start" doesn\'t contain a valid year format "YYYY"',
            status: 400
        })
    }
    if (params.year_end && !isValidYearFormat(params.year_end)) {
        throw new Error({
            details: 'parameter "year_end" doesn\'t contain a valid year format "YYYY"',
            status: 400
        })
    }
    return ({
        query: params.query,
        startDate: params.year_start || '',
        endDate: params.year_end || '',
        startIndex: params.start_index || 0,
        maxItems: params.max_items || 10
    })
}

const createCollectionResponse = (collection, fields) => {
    return {
        metadata: collection.metadata,
        data: collection.items.map(createItemReponse(fields))
    }
}

const getFieldName = () => {}

const createItemReponse = (fields) => (item) => {
    const attributes = fields.reduce((acc, curVal) => {
        return item.data[0][curVal]
            ? Object.assign({image_url: item.links[0].href }
                , acc
                , { [curVal]: item.data[0][curVal] })
            : acc
    }, {})
    return {
        type: 'images',
        id: item.data[0].nasa_id,
        attributes
    }
}

const getSearchResult = url => {
    return new Promise((resolve, reject) => {
        axios.get(url)
            .then(response => {
                const fields = ['date_created', 'image_url', 'title']
                return resolve(createCollectionResponse(response.data.collection, fields))
            })
            .catch(error => {
                return reject({
                    details: 'We don\'t feel like giving you back some data today, get back tomorrow',
                    status: 500
                })
            })
    })
}

const data = {
    "data": [
        {
            "type": "images",
            "id": "APOLLO 50th_FULL COLOR_300DPI",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/APOLLO 50th_FULL COLOR_300DPI/APOLLO 50th_FULL COLOR_300DPI~thumb.jpg",
                "date_created": "2018-06-18T00:00:00Z",
                "title": "APOLLO 50th_FULL COLOR_300DPI"
            }
        },
        {
            "type": "images",
            "id": "s69-35505",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/s69-35505/s69-35505~thumb.jpg",
                "date_created": "1969-06-03T00:00:00Z",
                "title": "APOLLO X - CREW"
            }
        },
        {
            "type": "images",
            "id": "S69-35503",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-35503/S69-35503~thumb.jpg",
                "date_created": "1969-06-03T00:00:00Z",
                "title": "APOLLO X - CREW TRAINING "
            }
        },
        {
            "type": "images",
            "id": "7995383",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/7995383/7995383~thumb.jpg",
                "date_created": "1979-05-01T00:00:00Z",
                "title": "Saturn Apollo Program"
            }
        },
        {
            "type": "images",
            "id": "S69-16402",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-16402/S69-16402~thumb.jpg",
                "date_created": "1968-12-29T00:00:00Z",
                "title": "Apollo 8"
            }
        },
        {
            "type": "images",
            "id": "S69-34968",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-34968/S69-34968~thumb.jpg",
                "date_created": "1969-05-25T00:00:00Z",
                "title": "INFLIGHT - APOLLO X "
            }
        },
        {
            "type": "images",
            "id": "S69-34969",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-34969/S69-34969~thumb.jpg",
                "date_created": "1969-05-25T00:00:00Z",
                "title": "INFLIGHT - APOLLO X "
            }
        },
        {
            "type": "images",
            "id": "S69-60662",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-60662/S69-60662~thumb.jpg",
                "date_created": "1969-12-01T00:00:00Z",
                "title": "Apollo 13 Emblem"
            }
        },
        {
            "type": "images",
            "id": "S69-33999",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-33999/S69-33999~thumb.jpg",
                "date_created": "1969-05-18T00:00:00Z",
                "title": "INFLIGHT - APOLLO X (CREW ACTIVITIES) "
            }
        },
        {
            "type": "images",
            "id": "S69-34313",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-34313/S69-34313~thumb.jpg",
                "date_created": "1969-05-20T00:00:00Z",
                "title": "INFLIGHT - APOLLO 10 (CREW ACTIVITIES) "
            }
        },
        {
            "type": "images",
            "id": "PIA13058",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/PIA13058/PIA13058~thumb.jpg",
                "date_created": "2010-04-16T17:10:24Z",
                "title": "Retracing the Steps of Apollo 15"
            }
        },
        {
            "type": "images",
            "id": "PIA14003",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/PIA14003/PIA14003~thumb.jpg",
                "date_created": "2011-02-05T07:00:24Z",
                "title": "New View of Apollo 14"
            }
        },
        {
            "type": "images",
            "id": "S68-56002",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S68-56002/S68-56002~thumb.jpg",
                "date_created": "1968-12-21T00:00:00Z",
                "title": "APOLLO VIII - LAUNCH - KSC "
            }
        },
        {
            "type": "images",
            "id": "S69-27915",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-27915/S69-27915~thumb.jpg",
                "date_created": "1969-03-11T00:00:00Z",
                "title": "Prelaunch - Apollo 10 (rollout) "
            }
        },
        {
            "type": "images",
            "id": "S68-26890",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S68-26890/S68-26890~thumb.jpg",
                "date_created": "1968-01-01T00:00:00Z",
                "title": "Apollo 6 (AS-502) Pacific Recovery"
            }
        },
        {
            "type": "images",
            "id": "S68-26936",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S68-26936/S68-26936~thumb.jpg",
                "date_created": "1968-01-01T00:00:00Z",
                "title": "Apollo 6 (AS-502) Pacific Recovery"
            }
        },
        {
            "type": "images",
            "id": "S68-26958",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S68-26958/S68-26958~thumb.jpg",
                "date_created": "1968-01-01T00:00:00Z",
                "title": "Apollo 6 (AS-502) Pacific Recovery"
            }
        },
        {
            "type": "images",
            "id": "S68-27161",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S68-27161/S68-27161~thumb.jpg",
                "date_created": "1968-01-01T00:00:00Z",
                "title": "Apollo 6 (AS-502) Pacific Recovery"
            }
        },
        {
            "type": "images",
            "id": "S68-27041",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S68-27041/S68-27041~thumb.jpg",
                "date_created": "1968-01-01T00:00:00Z",
                "title": "Apollo 6 (AS-502) Pacific Recovery"
            }
        },
        {
            "type": "images",
            "id": "S68-27076",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S68-27076/S68-27076~thumb.jpg",
                "date_created": "1968-01-01T00:00:00Z",
                "title": "Apollo 6 (AS-502) Pacific Recovery"
            }
        },
        {
            "type": "images",
            "id": "S69-27089",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-27089/S69-27089~thumb.jpg",
                "date_created": "1969-02-20T00:00:00Z",
                "title": " APOLLO 9 - PRELAUNCH (CDDT) - KSC "
            }
        },
        {
            "type": "images",
            "id": "200907200097HQ",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/200907200097HQ/200907200097HQ~thumb.jpg",
                "date_created": "2009-07-19T00:00:00Z",
                "title": "Apollo 40th Anniversary Press Conference"
            }
        },
        {
            "type": "images",
            "id": "S69-25478",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-25478/S69-25478~thumb.jpg",
                "date_created": "1969-02-23T00:00:00Z",
                "title": "Portrait - Apollo 9 "
            }
        },
        {
            "type": "images",
            "id": "200907200094HQ",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/200907200094HQ/200907200094HQ~thumb.jpg",
                "date_created": "2009-07-19T00:00:00Z",
                "title": "Apollo 40th Anniversary Press Conference"
            }
        },
        {
            "type": "images",
            "id": "200907200095HQ",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/200907200095HQ/200907200095HQ~thumb.jpg",
                "date_created": "2009-07-19T00:00:00Z",
                "title": "Apollo 40th Anniversary Press Conference"
            }
        },
        {
            "type": "images",
            "id": "200907200096HQ",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/200907200096HQ/200907200096HQ~thumb.jpg",
                "date_created": "2009-07-19T00:00:00Z",
                "title": "Apollo 40th Anniversary Press Conference"
            }
        },
        {
            "type": "images",
            "id": "S69-25862",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-25862/S69-25862~thumb.jpg",
                "date_created": "1969-03-03T00:00:00Z",
                "title": " LAUNCH - APOLLO 9 - CAPE "
            }
        },
        {
            "type": "images",
            "id": "S69-25861",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-25861/S69-25861~thumb.jpg",
                "date_created": "1969-03-03T00:00:00Z",
                "title": "LAUNCH - APOLLO 9 - CAPE "
            }
        },
        {
            "type": "images",
            "id": "S69-25881",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-25881/S69-25881~thumb.jpg",
                "date_created": "1969-03-03T00:00:00Z",
                "title": "Launch - Apollo 9 - KSC "
            }
        },
        {
            "type": "images",
            "id": "200907200098HQ",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/200907200098HQ/200907200098HQ~thumb.jpg",
                "date_created": "2009-07-19T00:00:00Z",
                "title": "Apollo 40th Anniversary Press Conference"
            }
        },
        {
            "type": "images",
            "id": "S69-19983",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-19983/S69-19983~thumb.jpg",
                "date_created": "1969-02-17T00:00:00Z",
                "title": " Crew Training - Apollo 9 - KSC "
            }
        },
        {
            "type": "images",
            "id": "S67-50903",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S67-50903/S67-50903~thumb.jpg",
                "date_created": "1967-09-11T00:00:00Z",
                "title": "Apollo 4 launch"
            }
        },
        {
            "type": "images",
            "id": "Maura_test_s69-36593",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/Maura_test_s69-36593/Maura_test_s69-36593~thumb.jpg",
                "date_created": "1969-05-26T00:00:00Z",
                "title": "Recovery - Apollo 10 "
            }
        },
        {
            "type": "images",
            "id": "S69-36593",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-36593/S69-36593~thumb.jpg",
                "date_created": "1969-05-26T00:00:00Z",
                "title": "Recovery - Apollo 10 "
            }
        },
        {
            "type": "images",
            "id": "S68-26668",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S68-26668/S68-26668~thumb.jpg",
                "date_created": "1968-06-01T00:00:00Z",
                "title": "Official Emblem - Apollo 7 - First (1st) Manned Apollo Space Mission"
            }
        },
        {
            "type": "images",
            "id": "as09-23-3596",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/as09-23-3596/as09-23-3596~thumb.jpg",
                "date_created": "1969-03-03T00:00:00Z",
                "title": "Apollo 9 Mission image - Scott in CM cabin "
            }
        },
        {
            "type": "images",
            "id": "S69-35099",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-35099/S69-35099~thumb.jpg",
                "date_created": "1969-05-25T00:00:00Z",
                "title": "ASTRONAUT LOVELL, JAMES A., JR. - APOLLO VIII (GUIDANCE & NAVIGATION [G&N])"
            }
        },
        {
            "type": "images",
            "id": "S67-49447",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S67-49447/S67-49447~thumb.jpg",
                "date_created": "1967-11-09T00:00:00Z",
                "title": "APOLLO SPACECRAFT 017 - RECOVERY - ATLANTIC"
            }
        },
        {
            "type": "images",
            "id": "200907200015HQ",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/200907200015HQ/200907200015HQ~thumb.jpg",
                "date_created": "2009-08-11T00:00:00Z",
                "title": "Apollo 40th Anniversary Press Conference"
            }
        },
        {
            "type": "images",
            "id": "S69-26148",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-26148/S69-26148~thumb.jpg",
                "date_created": "1969-03-06T00:00:00Z",
                "title": "Inflight - Apollo IX (Crew Activities) "
            }
        },
        {
            "type": "images",
            "id": "jsc2007e034221",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/jsc2007e034221/jsc2007e034221~thumb.jpg",
                "date_created": "1969-07-11T00:00:00Z",
                "title": "Apollo 11 spacecraft pre-launch"
            }
        },
        {
            "type": "images",
            "id": "9309375",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/9309375/9309375~thumb.jpg",
                "date_created": "1971-01-31T00:00:00Z",
                "title": "Saturn Apollo Program"
            }
        },
        {
            "type": "images",
            "id": "0102627",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/0102627/0102627~thumb.jpg",
                "date_created": "1972-04-01T00:00:00Z",
                "title": "Saturn Apollo Program"
            }
        },
        {
            "type": "images",
            "id": "s69-32614",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/s69-32614/s69-32614~thumb.jpg",
                "date_created": "1969-05-02T00:00:00Z",
                "title": "Apollo 10 portrait at KSC"
            }
        },
        {
            "type": "images",
            "id": "0101140",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/0101140/0101140~thumb.jpg",
                "date_created": "1967-01-01T00:00:00Z",
                "title": "Saturn Apollo Program"
            }
        },
        {
            "type": "images",
            "id": "0101493",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/0101493/0101493~thumb.jpg",
                "date_created": "1973-01-01T00:00:00Z",
                "title": "Saturn Apollo Program"
            }
        },
        {
            "type": "images",
            "id": "6761216",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/6761216/6761216~thumb.jpg",
                "date_created": "1967-11-09T00:00:00Z",
                "title": "Saturn Apollo Program"
            }
        },
        {
            "type": "images",
            "id": "S71-37963",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S71-37963/S71-37963~thumb.jpg",
                "date_created": "1971-06-28T00:00:00Z",
                "title": "Apollo 15 prime crew portrait"
            }
        },
        {
            "type": "images",
            "id": "200907200091HQ",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/200907200091HQ/200907200091HQ~thumb.jpg",
                "date_created": "2009-07-19T00:00:00Z",
                "title": "Apollo 40th Anniversary Morning Television"
            }
        },
        {
            "type": "images",
            "id": "200907200093HQ",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/200907200093HQ/200907200093HQ~thumb.jpg",
                "date_created": "2009-07-19T00:00:00Z",
                "title": "Apollo 40th Anniversary Morning Television"
            }
        },
        {
            "type": "images",
            "id": "S69-35507",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-35507/S69-35507~thumb.jpg",
                "date_created": "1969-06-03T00:00:00Z",
                "title": "Apollo 10 and 11 crews photographed during Apollo 10 debriefing"
            }
        },
        {
            "type": "images",
            "id": "S69-35504",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-35504/S69-35504~thumb.jpg",
                "date_created": "1969-06-03T00:00:00Z",
                "title": "Apollo 10 and 11 crews photographed during Apollo 10 debriefing"
            }
        },
        {
            "type": "images",
            "id": "6870605",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/6870605/6870605~thumb.jpg",
                "date_created": "1968-10-01T00:00:00Z",
                "title": "Saturn Apollo Program"
            }
        },
        {
            "type": "images",
            "id": "KSC-20170127-PH_KLS02_0005",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/KSC-20170127-PH_KLS02_0005/KSC-20170127-PH_KLS02_0005~thumb.jpg",
                "date_created": "2017-01-27T00:00:00Z",
                "title": "Apollo 1 Lessons Learned Show"
            }
        },
        {
            "type": "images",
            "id": "s69-34039",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/s69-34039/s69-34039~thumb.jpg",
                "date_created": "1969-05-18T00:00:00Z",
                "title": "Inflight - Apollo X - MSC "
            }
        },
        {
            "type": "images",
            "id": "9309295",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/9309295/9309295~thumb.jpg",
                "date_created": "1971-01-31T00:00:00Z",
                "title": "Saturn Apollo Program"
            }
        },
        {
            "type": "images",
            "id": "S69-62224",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-62224/S69-62224~thumb.jpg",
                "date_created": "1969-12-11T00:00:00Z",
                "title": "Apollo 13 - Prime Crew Portrait"
            }
        },
        {
            "type": "images",
            "id": "S72-50438",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S72-50438/S72-50438~thumb.jpg",
                "date_created": "1971-09-30T00:00:00Z",
                "title": "Apollo 17 prime crew portrait"
            }
        },
        {
            "type": "images",
            "id": "S67-49423",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S67-49423/S67-49423~thumb.jpg",
                "date_created": "1967-11-09T00:00:00Z",
                "title": "Recovery - Apollo Spacecraft (S/C)-017"
            }
        },
        {
            "type": "images",
            "id": "S71-16637",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S71-16637/S71-16637~thumb.jpg",
                "date_created": "1971-01-27T00:00:00Z",
                "title": "Plaque the Apollo 14 crew will leave on the Moon"
            }
        },
        {
            "type": "images",
            "id": "S66-36742",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S66-36742/S66-36742~thumb.jpg",
                "date_created": "1966-12-01T00:00:00Z",
                "title": "APOLLO-SATURN (AS)-204 INSIGNIA - MSC"
            }
        },
        {
            "type": "images",
            "id": "S69-18569",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-18569/S69-18569~thumb.jpg",
                "date_created": "1969-02-06T00:00:00Z",
                "title": "Emblem - Apollo 9 Space Mission"
            }
        },
        {
            "type": "images",
            "id": "KSC-20170127-PH_KLS02_0082",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/KSC-20170127-PH_KLS02_0082/KSC-20170127-PH_KLS02_0082~thumb.jpg",
                "date_created": "2017-01-27T00:00:00Z",
                "title": "Apollo 1 Lessons Learned Show"
            }
        },
        {
            "type": "images",
            "id": "as12-47-6918",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/as12-47-6918/as12-47-6918~thumb.jpg",
                "date_created": "1969-11-19T00:00:00Z",
                "title": "Apollo 12 Mission image - View of part of the deployed Apollo Lunar Surface Experiment Package (ALSEP) "
            }
        },
        {
            "type": "images",
            "id": "KSC-20170127-PH_KLS02_0010",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/KSC-20170127-PH_KLS02_0010/KSC-20170127-PH_KLS02_0010~thumb.jpg",
                "date_created": "2017-01-27T00:00:00Z",
                "title": "Apollo 1 Lessons Learned Show"
            }
        },
        {
            "type": "images",
            "id": "KSC-20170127-PH_KLS02_0014",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/KSC-20170127-PH_KLS02_0014/KSC-20170127-PH_KLS02_0014~thumb.jpg",
                "date_created": "2017-01-27T00:00:00Z",
                "title": "Apollo 1 Lessons Learned Show"
            }
        },
        {
            "type": "images",
            "id": "KSC-20170127-PH_KLS02_0078",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/KSC-20170127-PH_KLS02_0078/KSC-20170127-PH_KLS02_0078~thumb.jpg",
                "date_created": "2017-01-27T00:00:00Z",
                "title": "Apollo 1 Lessons Learned Show"
            }
        },
        {
            "type": "images",
            "id": "KSC-20170127-PH_KLS02_0103",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/KSC-20170127-PH_KLS02_0103/KSC-20170127-PH_KLS02_0103~thumb.jpg",
                "date_created": "2017-01-27T00:00:00Z",
                "title": "Apollo 1 Lessons Learned Show"
            }
        },
        {
            "type": "images",
            "id": "KSC-20170127-PH_KLS02_0031",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/KSC-20170127-PH_KLS02_0031/KSC-20170127-PH_KLS02_0031~thumb.jpg",
                "date_created": "2017-01-27T00:00:00Z",
                "title": "Apollo 1 Lessons Learned Show"
            }
        },
        {
            "type": "images",
            "id": "S69-38992",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-38992/S69-38992~thumb.jpg",
                "date_created": "1969-09-01T00:00:00Z",
                "title": "Portraits - Apollo 12 - MSC "
            }
        },
        {
            "type": "images",
            "id": "S71-30463",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S71-30463/S71-30463~thumb.jpg",
                "date_created": "1971-05-04T00:00:00Z",
                "title": "Apollo 15 insignia"
            }
        },
        {
            "type": "images",
            "id": "S69-52336",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-52336/S69-52336~thumb.jpg",
                "date_created": "1969-09-22T00:00:00Z",
                "title": "Official insignia of Apollo 12"
            }
        },
        {
            "type": "images",
            "id": "jsc2007e045377",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/jsc2007e045377/jsc2007e045377~thumb.jpg",
                "date_created": "2016-06-16T00:00:00Z",
                "title": "Panoramas of Apollo sites "
            }
        },
        {
            "type": "images",
            "id": "S67-36022",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S67-36022/S67-36022~thumb.jpg",
                "date_created": "1967-06-20T00:00:00Z",
                "title": "APOLLO SPACECRAFT 017 - VERTICAL ASSEMBLY BLDG. (VAB) - KSC"
            }
        },
        {
            "type": "images",
            "id": "7004739",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/7004739/7004739~thumb.jpg",
                "date_created": "1972-12-18T00:00:00Z",
                "title": "Saturn Apollo Program"
            }
        },
        {
            "type": "images",
            "id": "7031833",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/7031833/7031833~thumb.jpg",
                "date_created": "1972-04-21T00:00:00Z",
                "title": "Saturn Apollo Program"
            }
        },
        {
            "type": "images",
            "id": "0101489",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/0101489/0101489~thumb.jpg",
                "date_created": "1976-06-01T00:00:00Z",
                "title": "Saturn Apollo Program"
            }
        },
        {
            "type": "images",
            "id": "KSC-20170127-PH_KLS01_0103",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/KSC-20170127-PH_KLS01_0103/KSC-20170127-PH_KLS01_0103~thumb.jpg",
                "date_created": "2017-01-27T00:00:00Z",
                "title": "Apollo 1 Tribute Opening"
            }
        },
        {
            "type": "images",
            "id": "200907200099HQ",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/200907200099HQ/200907200099HQ~thumb.jpg",
                "date_created": "2009-07-19T00:00:00Z",
                "title": "Apollo 40th Anniversary Press Conference"
            }
        },
        {
            "type": "images",
            "id": "200907200100HQ",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/200907200100HQ/200907200100HQ~thumb.jpg",
                "date_created": "2009-07-19T00:00:00Z",
                "title": "Apollo 40th Anniversary Press Conference"
            }
        },
        {
            "type": "images",
            "id": "KSC-20170127-PH_KLS01_0083",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/KSC-20170127-PH_KLS01_0083/KSC-20170127-PH_KLS01_0083~thumb.jpg",
                "date_created": "2017-01-27T00:00:00Z",
                "title": "Apollo 1 Tribute Opening"
            }
        },
        {
            "type": "images",
            "id": "KSC-20170127-PH_KLS01_0040",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/KSC-20170127-PH_KLS01_0040/KSC-20170127-PH_KLS01_0040~thumb.jpg",
                "date_created": "2017-01-27T00:00:00Z",
                "title": "Apollo 1 Tribute Opening"
            }
        },
        {
            "type": "images",
            "id": "KSC-20170127-PH_KLS01_0071",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/KSC-20170127-PH_KLS01_0071/KSC-20170127-PH_KLS01_0071~thumb.jpg",
                "date_created": "2017-01-27T00:00:00Z",
                "title": "Apollo 1 Tribute Opening"
            }
        },
        {
            "type": "images",
            "id": "S69-34476",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-34476/S69-34476~thumb.jpg",
                "date_created": "1969-05-21T00:00:00Z",
                "title": "Inflight - Apollo 10 "
            }
        },
        {
            "type": "images",
            "id": "6973136",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/6973136/6973136~thumb.jpg",
                "date_created": "1968-12-27T00:00:00Z",
                "title": "Saturn Apollo Program"
            }
        },
        {
            "type": "images",
            "id": "6903633",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/6903633/6903633~thumb.jpg",
                "date_created": "1969-11-23T00:00:00Z",
                "title": "Saturn Apollo Program"
            }
        },
        {
            "type": "images",
            "id": "200907200082HQ",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/200907200082HQ/200907200082HQ~thumb.jpg",
                "date_created": "2009-07-19T00:00:00Z",
                "title": "Apollo 40th Newseum Panel Discussion"
            }
        },
        {
            "type": "images",
            "id": "200907200083HQ",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/200907200083HQ/200907200083HQ~thumb.jpg",
                "date_created": "2009-07-19T00:00:00Z",
                "title": "Apollo 40th Newseum Panel Discussion"
            }
        },
        {
            "type": "images",
            "id": "KSC-20170127-PH_KLS02_0084",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/KSC-20170127-PH_KLS02_0084/KSC-20170127-PH_KLS02_0084~thumb.jpg",
                "date_created": "2017-01-27T00:00:00Z",
                "title": "Apollo 1 Lessons Learned Show"
            }
        },
        {
            "type": "images",
            "id": "KSC-20170127-PH_KLS02_0053",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/KSC-20170127-PH_KLS02_0053/KSC-20170127-PH_KLS02_0053~thumb.jpg",
                "date_created": "2017-01-27T00:00:00Z",
                "title": "Apollo 1 Lessons Learned Show"
            }
        },
        {
            "type": "images",
            "id": "KSC-20170127-PH_KLS02_0086",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/KSC-20170127-PH_KLS02_0086/KSC-20170127-PH_KLS02_0086~thumb.jpg",
                "date_created": "2017-01-27T00:00:00Z",
                "title": "Apollo 1 Lessons Learned Show"
            }
        },
        {
            "type": "images",
            "id": "S68-50977",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S68-50977/S68-50977~thumb.jpg",
                "date_created": "1968-11-20T00:00:00Z",
                "title": "Crew Training - Apollo IX (Egress) - Gulf "
            }
        },
        {
            "type": "images",
            "id": "as09-24-3652",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/as09-24-3652/as09-24-3652~thumb.jpg",
                "date_created": "1969-03-03T00:00:00Z",
                "title": "Apollo 9 Mission image - Command Module "
            }
        },
        {
            "type": "images",
            "id": "S68-50960",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S68-50960/S68-50960~thumb.jpg",
                "date_created": "1968-11-20T00:00:00Z",
                "title": " Crew Training - Apollo IX (Egress) - Gulf "
            }
        },
        {
            "type": "images",
            "id": "S68-50967",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S68-50967/S68-50967~thumb.jpg",
                "date_created": "1968-11-20T00:00:00Z",
                "title": "Crew Training - Apollo IX (Egress) - Gulf "
            }
        },
        {
            "type": "images",
            "id": "as09-24-3657",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/as09-24-3657/as09-24-3657~thumb.jpg",
                "date_created": "1969-03-03T00:00:00Z",
                "title": "Apollo 9 Mission image - Command Module"
            }
        },
        {
            "type": "images",
            "id": "200907200085HQ",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/200907200085HQ/200907200085HQ~thumb.jpg",
                "date_created": "2009-07-19T00:00:00Z",
                "title": "Apollo 40th Newseum Panel Discussion"
            }
        },
        {
            "type": "images",
            "id": "7008012",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/7008012/7008012~thumb.jpg",
                "date_created": "1970-01-01T00:00:00Z",
                "title": "Saturn Apollo Program"
            }
        },
        {
            "type": "images",
            "id": "S69-34143",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/S69-34143/S69-34143~thumb.jpg",
                "date_created": "1969-05-18T00:00:00Z",
                "title": "APOLLO X - LAUNCH - PAD 39B - KSC "
            }
        },
        {
            "type": "images",
            "id": "6890492",
            "attributes": {
                "image_url": "https://images-assets.nasa.gov/image/6890492/6890492~thumb.jpg",
                "date_created": "1968-12-19T00:00:00Z",
                "title": "Saturn Apollo Program"
            }
        }
    ]
}

const buildParameter = (paramName, paramVal) => `&${paramName}=${encodeURIComponent(paramVal)}`
const buildQueryStr = (params) => {
    const { year_start, year_end } = params
    return '&media_type=image'
        + (year_start ? buildParameter('year_start', year_start) : '')
        + (year_end ? buildParameter('year_end', year_end) : '')
}

const buildApiSearchUrl = (params) => {
    const baseURL = `${NASA_SEARCH_ENDPOINT}?q=${params.query}` 
    return baseURL + buildQueryStr(params)
}

router.get('/', async (req, res) => {
    try {
        const params = extractParams(req.query)
        const nasaQuery = buildApiSearchUrl(params)
        const searchResult = await getSearchResult(nasaQuery)
        return res.json(searchResult)
    } catch (e) {
        res.status(500)
        return res.json(e)
    }
})

export default router