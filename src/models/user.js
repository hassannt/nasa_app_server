const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/my_database');

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const UserSchema = new Schema({
    id: ObjectId,
    email: { type: String, require: true },
    hashPassword: { required: true, type: String }
});

const UserModal = mongoose.model('User', UserSchema);

module.exports = {
    User
}