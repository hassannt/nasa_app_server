import jwt from 'jsonwebtoken';

export function generateToken(email) {
    return jwt.sign({
        email
    }, 'secret');
}