import dotenv from 'dotenv'
dotenv.load()

const MongoClient = require('mongodb').MongoClient;

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = process.env.DB_NAME
const collectionName = process.env.DB_COLLECTION_NAME

import { compareHash, hashPassword } from './hash_util'

export async function getUserByEmail(email) {
    return new Promise((resolve, reject) => {
        MongoClient.connect(url, function (err, client) {
            const db = client.db(dbName);
            const collection = db.collection('users');
            collection.find({ email }).toArray(function (err, docs) {
                client.close()
                if (err) {
                    return reject({
                        status: 500,
                        error: 'An Error occured while trying to fetch data from the db'
                    })
                }
                return resolve(docs.length
                    ? docs[0]
                    : null
                )
            });
        });
    })
}

export async function updateUser(email, propName, newData) {
    const user = await getUserByEmail(email)
    if (!user)
        throw { status: 400, details: 'A user with this email address has not been found' }
    MongoClient.connect(url,  (err, client) => {
        const collection = client.db(dbName).collection(collectionName);
        collection.updateOne({ email }, {
            $set: { [propName]: newData }
        })
        client.close()
    });
}

export async function createUser(userData) {
    const userExists = !!(await getUserByEmail(userData.email))
    if (userExists) throw { status: 500, details: 'A user with this email is already registered' }
    return new Promise((resolve, reject) => {
        MongoClient.connect(url, function (err, client) {
            const collection = client.db(dbName).collection(collectionName);
            collection.insertOne(userData, (err, records) => {
                client.close()
                if (err) return reject({
                    status: 500,
                    details: 'An Error occured while trying to fetch data from the db'
                })
                return resolve(records)
            });;
        });
    })
}

export async function authenticateUser(email, password) {
    const user = await getUserByEmail(email)
    if (!user) throw {
        status: 400,
        details: 'Ingen användare har hittas med detta e-post'
    }
    const result = await compareHash(password, user.password)
    if (!result) throw {
        status: 400,
        details: 'Incorrect password'
    }
    return user
}
