import express from 'express'

import { updateUser } from '../lib/user_util.js'
import { getUserByEmail } from '../lib/user_util.js';
import Axios from 'axios';
import { resolve } from 'path';

const router = express.Router()

const NASA_ASSET_URL = 'https://images-api.nasa.gov/asset'

const extractParams = params => {
    const errors = []
    if (!params.favourites) {
        errors.push({
            error: 'Mandatory parameter "favourites" is missing',
            status: 400
        })
    }
    if (errors.length)
        throw errors
    return params
}

const getFavourites = favourites => {
    return Promise.all(favourites.map(favourite => {
        return new Promise((resolve, reject) => {
            Axios.get(`${NASA_ASSET_URL}/${favourite}`)
                .then(res => {
                    const items = res.data.collection.items;
                    const thumbnail_url = items.find(item => item.href.indexOf('thumb.jpg') !== -1)
                    resolve({
                        type: 'images',
                        id: favourite,
                        attributes: {
                            image_url: thumbnail_url.href
                        }
                    })
                })
                .catch(e => reject(e))
        })
    }))
}

router.get('/favourites', async (req, res) => {
    try {
        const user = await getUserByEmail(req.userEmail)
        const favourites = await getFavourites(user.favourites)
        return res.json({
            data: favourites
        })
    } catch (e) {
        return res.status(e.status || 500).json({ error: e })
    }
})

router.patch('/favourites', async (req, res) => {
    try {
        const params = extractParams(req.body)
        await updateUser(req.userEmail, 'favourites', params.favourites)
        const favourites = await getFavourites(params.favourites)
        return res.json({
            data: favourites
        })
    } catch (e) {
        return res.status(e.status || 500).json({ error: e })
    }
})

export default router