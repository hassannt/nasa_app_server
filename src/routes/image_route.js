import express from 'express'
import axios from 'axios'

import { updateUser } from '../lib/user_util.js'

const router = express.Router()

const NASA_METADATA_URL = 'https://images-api.nasa.gov/metadata'
const NASA_ASSET_URL = 'https://images-api.nasa.gov/asset'

const extractParams = params => {
    const errors = []
    if (!params.favourites) {
        errors.push({
            error: 'Mandatory parameter "favourites" is missing',
            status: 400
        })
    }
    if (errors.length)
        throw errors
    return params
}

router.get('/:id', async (req, res) => {
    try {
        axios.get(`${NASA_ASSET_URL}/${req.params.id}`)
        .then(resp => {
            const items = resp.data.collection.items;
            const thumbnail_url = items.find(item => item.href.indexOf('thumb.jpg') !== -1)
            const metadataUrl = items.find(item => item.href.indexOf('metadata.json') !== -1)
            axios.get(metadataUrl.href)
            .then(resp => {
                res.json({
                    data: {
                        type: 'images',
                        id: req.params.id,
                        attributes: {
                            image_url: thumbnail_url.href,
                            metadata: resp.data
                        }
                    }
                })
            })
        })
        return true
    } catch (e) {
        return res.status(e.status || 500).json({ error: e })
    }
})

export default router