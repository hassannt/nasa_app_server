import jwt from 'jsonwebtoken'
function getToken(req) {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer')
        return req.headers.authorization.split(' ')[1];
    else if (req.query && req.query.token)
        return req.query.token;
    return null;
}

export default function verifyToken(req, res, next) {
    if (req.path === '/login' || req.path === '/register' || req.path.indexOf('/images') > -1)
        next()
    else {
        const token = getToken(req);
    if (!token) {
        return res.status(401).send({
            error: {
                status: 401,
                details: 'No token has been provided'
            }
        });
    } 
    jwt.verify(token, 'secret', (err, decoded) => {
        if (err)
            return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
        req.userEmail = decoded.email;
        next();
    });   
    }
}