import express from 'express'
import bodyParser from 'body-parser'
import dotenv from 'dotenv'

dotenv.load()

const app = express()

const port = 3000
app.use(bodyParser.json())

import SearchRoute from './routes/search_route'
import RegisterationRoute from './routes/registeration_route'
import LoginRoute from './routes/login_route'
import UserRoute from './routes/user_route'
import MeRoute from './routes/me_route'
import ImageRoute from './routes/image_route'
import TokenMw from './middlewares/token_mw'
import cache from 'express-cache-route';

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    if (req.method === 'OPTIONS') return res.send(200)
    next();
});

app.use(TokenMw)
app.use('/me', MeRoute)
app.use('/register', RegisterationRoute)
app.use('/login', LoginRoute)
app.use('/search', SearchRoute)
app.use('/user', UserRoute)
app.use('/images', ImageRoute)

app.listen(port, () => console.log(`The cool app is listening on port ${port}!`)) 