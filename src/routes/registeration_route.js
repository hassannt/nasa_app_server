import express from 'express'

import { hashPassword } from '../lib/hash_util.js'
import { createUser } from '../lib/user_util.js'

const router = express.Router()

const extractParams = params => {
    const errors = []
    if (!params.email) {
        errors.push({
            error: 'Mandatory parameter "email" is missing',
            status: 400
        })
    }
    if (!params.password) {
        errors.push({
            error: 'Mandatory parameter "password" is missing',
            status: 400
        })
    }
    if (errors.length)
        throw errors
    return params
}

const buildUserData = async (params) => {
        const pass = await hashPassword(params.password)
        const obj = {
            email: params.email,
            firstName: params.firstName,
            lastName: params.lastName,
            password: pass,
            favourites: []
        }
        return obj
}
 
router.post('/', async (req, res) => {
    try {
        const params = extractParams(req.body)
        const userData = await buildUserData(params)
        await createUser(userData)
        return res.json({
            message: 'User has successfully been registered'
        })
    } catch (e) {
        return res.status(e.status || 500).json({ error: e })
    }
})

export default router